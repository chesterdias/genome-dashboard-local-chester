// Code runs locally on http://localhost:3000/ defined in bin/www
let fetch = require('node-fetch');

const SPECIES = 'https://bitbucket.org/lorainelab/integrated-genome-browser/raw/master/core/synonym-lookup/src/main/resources/species.txt';
const SYNONYMS = 'https://bitbucket.org/lorainelab/integrated-genome-browser/raw/master/core/synonym-lookup/src/main/resources/synonyms.txt';
const ALLQUICKLOADS = 'https://bitbucket.org/lorainelab/integrated-genome-browser/raw/master/core/igb-preferences/src/main/resources/igbDefaultPrefs.json';

// Fetch data (Genome version name, Scientific Name) from the factory urls
// Return a list, of which a sample element is:
// 'A_thaliana_Jun_2009\tArabidopsis thaliana TAIR9'
let mergeAllQuickLoadSites = async () => {
        try{
            const response = await fetch(ALLQUICKLOADS);
            const tempQuickData = await response.json();
            let quickLoadData = tempQuickData.prefs.server;
            let CompleteFactoryData = [];
            for (const factory of quickLoadData) {
                if(factory.factoryName === 'quickload') {
                    try {
                        let tempData = await fetch(factory.url + '/contents.txt');
                        let data = await tempData.text();
                        let finalData = data.split('\n');
                        CompleteFactoryData = [...CompleteFactoryData, ...finalData]
                    } catch (error) {
                        console.log(error.message)
                    }
                }
            }
            return CompleteFactoryData;
        } catch (error) {
            console.log(error.message)
        }
};

//Derive the genome version prefix - to be used as a key for every dictionary data structure used
//Genome version name contains Month and year as last 2 strings separated by underscore
//Genome Version prefix is derived by removing the month and year.
function getGenomeVersionPrefix(elementData){
    let temp = elementData.slice(0,elementData.lastIndexOf('_'));//Remove the year
    temp = temp.slice(0,temp.lastIndexOf('_'));//Remove Month
    return temp;
}

//Get all the data need for rendering dashboard
var getContent = async () => {
    // Dictionary data structure used to hold the content data to be displayed on ./views/index.ejs
    let genomeData = {};
    let commonName = {};
    let scientificName = {};
    let synonyms = {};

    // Build the dictionary values for genomeData based on a fixed key used in every dictionary
    try {
        let content_arr = await mergeAllQuickLoadSites();
        content_arr.forEach(element => {
            if (element.trim().length > 0) {
                let final=getGenomeVersionPrefix(element.split('\t')[0]);
                if (!final.includes(undefined)) {
                    temp = element.split('\t')[1].split(' ')[0] + ' ' + element.split('\t')[1].split(' ')[1];
                    if (genomeData[final] !== undefined) {
                        if(!genomeData[final].includes(element.split('\t')[0])){
                            genomeData[final].push(element.split('\t')[0]);
                        }
                    } else {
                        genomeData[final] = [element.split('\t')[0]];
                    }
                }
            }
        });
    } catch (error) {
        console.log(error.message);
    }

    //Fetch data from the SPECIES link to create commonName using the keys similar to above,
    //When keys are not found to be present they will be added as new key, new entries.
    try {
        await fetch(SPECIES)
            .then((response) => response.text())
            .then(newResp => {
                let content_arr = newResp.split('\n');
                let genomeKeys = Object.keys(genomeData)
                content_arr.forEach(element => {
                    if (element.trim().length > 0) {
                        let splitElement = element.split('\t');
                        let bionames = splitElement.filter(data => data.includes('_'));
                        let final = bionames[0].replace(/\r/g, '').trim();
                        // Use merged quickload site data as ultimate dictionary key name authority.
                        // - Required because some genomes in species.txt differ in name from those
                        // returned by mergeAllQuickLoadSites() e.g. 'V_corymbosum' differs from
                        // 'V_corymbosum_scaffold'. In these cases, this assumes that the genome
                        // names returned by mergeAllQuickLoadSites() are longer/more descriptive.
                        genomeKeys.forEach(key => {
                          if (key.includes(final)) {
                            final = key
                          }
                        });
                        if (!final.includes(undefined)) {
                            if (commonName[final] !== undefined) {
                                commonName[final].push(splitElement[1][0].toUpperCase() + esplitElement[1].substr(1))
                            } else {
                                commonName[final] = [splitElement[1][0].toUpperCase() + splitElement[1].substr(1)]
                            }
                            scientificName[final] = [splitElement[0][0].toUpperCase() + splitElement[0].substr(1)]
                        }
                    }
                });
            });
    } catch (error) {
        console.log(error.message);
    }

    //Fetch synonyms for the i button which appears on clicking genome versions for each image
    try {
        await fetch(SYNONYMS)
            .then((response) => response.text())
            .then(newResp => {
                let content_arr = newResp.split('\n');
                content_arr.forEach(element => {
                    let temp_name = element.split('\t')[0];
                    if (!temp_name.includes(undefined)) {
                        if (synonyms[temp_name] !== undefined) {
                            synonyms[temp_name].push(element.split('\t').slice(1));
                        } else {
                            synonyms[temp_name] = element.split('\t').slice(1);
                        }
                    }
                });
            });
    } catch (error) {
        console.log(error.message);
    }

    //Sort genomeData dict applying sort on keys
    //The keys of genomeData dict are used for iterating through all the other dictionary while rendering index.ejs
    //Once keys are sorted all the species displayed will also be sorted on the UI
    sorted_genomeData = Object.keys(genomeData)
        .sort().reduce(function(Obj, key) {
            Obj[key] = genomeData[key];
            return Obj;
        }, {});
    return {'genomeData': sorted_genomeData, 'commonName': commonName, 'scientificName': scientificName, 'Synonyms':synonyms};
};

module.exports.getContent = getContent;
