let IGBURL = 'http://127.0.0.1:7085/igbStatusCheck';

let igbIsRunning = async () => {
  try {
    console.log(await fetch(IGBURL));
    return true;
  } catch (err) {
    console.log(err);
    return false;
  }
}

let igbRedirect = (ev, data) => {
    if (ev.target.className == 'material-icons') {
      return
    }
    igbIsRunning().then(running => {
      if (!running) {
          $('#myModal').modal('toggle');
      } else {
        window.location = 'http://localhost:7085/IGBControl?version=' + data;
        setTimeout((data) => {
          window.location = 'http://localhost:7085/bringIGBToFront';
        }, 250);
      }
    })
};
